/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
   * @namespace product
 */

theme.Product = (function() {

  var selectors = {
    addToCart: '[data-add-to-cart]',
    addToCartText: '[data-add-to-cart-text]',
    comparePrice: '[data-compare-price]',
    comparePriceText: '[data-compare-text]',
    originalSelectorId: '[data-product-select]',
    priceWrapper: '[data-price-wrapper]',
    productFeaturedImage: '[data-product-featured-image]',
    productJson: '[data-product-json]',
    productPrice: '[data-product-price]',
    productThumbs: '[data-product-single-thumbnail]',
    singleOptionSelector: '[data-single-option-selector]',
    productQuantity:'[data-product-quantity]'
  };

  /**
   * Product section constructor. Runs on page load as well as Theme Editor
   * `section:load` events.
   * @param {string} container - selector for the section container DOM element
   */
  function Product(container) {
    this.$container = $(container);

    // Stop parsing if we don't have the product json script tag when loading
    // section in the Theme Editor
    if (!$(selectors.productJson, this.$container).html()) {
      return;
    }

    var sectionId = this.$container.attr('data-section-id');
    this.productSingleObject = JSON.parse($(selectors.productJson, this.$container).html());

    var options = {
      $container: this.$container,
      enableHistoryState: this.$container.data('enable-history-state') || false,
      singleOptionSelector: selectors.singleOptionSelector,
      originalSelectorId: selectors.originalSelectorId,
      product: this.productSingleObject
    };

    this.settings = {};
    this.namespace = '.product';
    this.variants = new slate.Variants(options);
    this.$featuredImage = $(selectors.productFeaturedImage, this.$container);

    this.$container.on('variantChange' + this.namespace, this.updateAddToCartState.bind(this));
    this.$container.on('variantPriceChange' + this.namespace, this.updateProductPrices.bind(this));

    if (this.$featuredImage.length > 0) {
      this.settings.imageSize = slate.Image.imageSize(this.$featuredImage.attr('src'));
      slate.Image.preload(this.productSingleObject.images, this.settings.imageSize);

      this.$container.on('variantImageChange' + this.namespace, this.updateProductImage.bind(this));
    }

    this.quantityChange(this);
    this.imagesCarousel(this);
    this.expandCollapseDescription(this);
  }

  Product.prototype = $.extend({}, Product.prototype, {

    /**
     * Updates the DOM state of the add to cart button
     *
     * @param {boolean} enabled - Decides whether cart is enabled or disabled
     * @param {string} text - Updates the text notification content of the cart
     */
    updateAddToCartState: function(evt) {
      var variant = evt.variant;

      if (variant) {
        $(selectors.priceWrapper, this.$container).removeClass('hide');
      } else {
        $(selectors.addToCart, this.$container).prop('disabled', true);
        $(selectors.addToCartText, this.$container).html(theme.strings.unavailable);
        $(selectors.priceWrapper, this.$container).addClass('hide');
        return;
      }

      if (variant.available) {
        $(selectors.addToCart, this.$container).prop('disabled', false);
        $(selectors.addToCartText, this.$container).html(theme.strings.addToCart);
      } else {
        $(selectors.addToCart, this.$container).prop('disabled', true);
        $(selectors.addToCartText, this.$container).html(theme.strings.soldOut);
      }
    },

    /**
     * Updates the DOM with specified prices
     *
     * @param {string} productPrice - The current price of the product
     * @param {string} comparePrice - The original price of the product
     */
    updateProductPrices: function(evt) {
      var variant = evt.variant;
      var $comparePrice = $(selectors.comparePrice, this.$container);
      var $compareEls = $comparePrice.add(selectors.comparePriceText, this.$container);

      $(selectors.productPrice, this.$container).html(slate.Currency.formatMoney(variant.price, theme.moneyFormat));

      if (variant.compare_at_price > variant.price) {
        $comparePrice.html(slate.Currency.formatMoney(variant.compare_at_price, theme.moneyFormat));
        $compareEls.removeClass('hide');
      } else {
        $comparePrice.html('');
        $compareEls.addClass('hide');
      }
    },

    /**
     * Updates the DOM with the specified image URL
     *
     * @param {string} src - Image src URL
     */
    updateProductImage: function(evt) {
      var variant = evt.variant;
      var sizedImgUrl = slate.Image.getSizedImageUrl(variant.featured_image.src, this.settings.imageSize);

      this.$featuredImage.attr('src', sizedImgUrl);
    },

    /**
     * Event callback for Theme Editor `section:unload` event
     */
    onUnload: function() {
      this.$container.off(this.namespace);
    },

    quantityChange: function() {
      var input = $(selectors.productQuantity, this.$container);
      var buttonMinus = input.siblings('.minus');
      var buttonPlus = input.siblings('.plus');

      buttonMinus.on('click', function(event){ 
        var curVal = input.val();
        event.preventDefault();
        curVal > 1 ? input.attr("value", parseInt( curVal ) - 1 ) : 0;
      });
      buttonPlus.on('click', function(event){
        var curVal = input.val();
        event.preventDefault();
        input.attr("value", parseInt( curVal ) + 1 );
      });
    },

    imagesCarousel: function(){
      $('.product__image-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        dots: true,
        mobileFirst: true,
        responsive: [{    
          breakpoint: 750,
          settings: {
            dots: false,
            asNavFor: '.product__image-slider-nav'
          }    
        }]
      });
      $('.product__image-slider-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        dots: false,
        focusOnSelect: true,
        infinite: false,
        arrows: false,
        asNavFor: '.product__image-slider'
      });
    },

    expandCollapseDescription: function(){
      var controller = $('.collapse-control');   
      // slate.utils.collapseSection(targetElm); 
      
      controller.on('click', function(e){
        e.preventDefault();
        var targetElm = $('.'+$(this).attr('collapse-target'));
        // var content = targetElm.find('product__description');
        console.log(targetElm);
  
        if (targetElm.hasClass('open')){
          targetElm.removeClass('open');
          $(this).text('Show More');
          // collapseSection(targetElm);
        }
        else{
          targetElm.addClass('open');
          $(this).text('Show Less');
          // expandSection(targetElm);
        }
      });
    },
  
    collapseSection: function(element){
      // get the height of the element's inner content, regardless of its actual size
      var sectionHeight = element.scrollHeight;
      
      // temporarily disable all css transitions
      var elementTransition = element.style.transition;
      element.style.transition = '';
      
      // on the next frame (as soon as the previous style change has taken effect),
      // explicitly set the element's height to its current pixel height, so we 
      // aren't transitioning out of 'auto'
      requestAnimationFrame(function() {
        element.style.height = sectionHeight + 'px';
        element.style.transition = elementTransition;
        
        // on the next frame (as soon as the previous style change has taken effect),
        // have the element transition to height: 0
        requestAnimationFrame(function() {
          element.style.height = 200 + 'px';
        });
      });
      
      // mark the section as "currently collapsed"
      element.setAttribute('data-collapsed', 'true');
    },
  
    expandSection: function(element){
      // get the height of the element's inner content, regardless of its actual size
      var sectionHeight = element.scrollHeight;
      
      // have the element transition to the height of its inner content
      element.style.height = sectionHeight + 'px';
  
      // when the next css transition finishes (which should be the one we just triggered)
      element.addEventListener('transitionend', function(e) {
        // remove this event listener so it only gets triggered once
        element.removeEventListener('transitionend', arguments.callee);
        
        // remove "height" from the element's inline styles, so it can return to its initial value
        element.style.height = null;
      });
      
      // mark the section as "currently not collapsed"
      element.setAttribute('data-collapsed', 'false');
    }

  });

  return Product;
})();

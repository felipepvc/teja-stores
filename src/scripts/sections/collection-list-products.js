var collectionFunctions = {

    tabs: $('.collection-list-products__tabs h3'),
    container: $('.collections-container .collection-list'),

    tabsController: function(){
        collectionFunctions.hideContent( collectionFunctions.container );
        collectionFunctions.tabs.first().addClass('active');
        collectionFunctions.showContent( collectionFunctions.container.first() );
        collectionFunctions.makeSlick( collectionFunctions.container.first() );
        
        collectionFunctions.tabs.on('click', function(){

            var target = $(this).attr('data-target');
            var content = $("[data-collection-id='"+target+"']");
            $('.slick-slider').slick('unslick');

            if( $(this).hasClass('active') == false ){
                collectionFunctions.tabs.removeClass('active');
                $(this).addClass('active');
                collectionFunctions.hideContent( collectionFunctions.container );
                collectionFunctions.showContent( content );
                collectionFunctions.makeSlick( content );
            }
        });
    },

    makeSlick: function(element){
        element.slick({
            mobileFirst: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false,
            responsive: [{    
                breakpoint: 750,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    dots: false,
                    arrows: true
                }   
            }]
        });
    },

    hideContent: function(element){
        element.addClass('small--hide medium-up--hide');
    },

    showContent: function(element){
        element.removeClass('small--hide medium-up--hide');
    }

}

collectionFunctions.tabsController();
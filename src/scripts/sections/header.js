var headerFunctions = {

    bt_menuOpen: $('.header__hamburger'),
    bt_menuClose: $('.header__menu-close'),
    bt_search: $('.header__search-button'),
    searchForm: $('.header__nav .header__search'),
    mainMenu: $('.header__main-menu'),

    openCloseMenu: function () {

        headerFunctions.bt_menuOpen.on('click', function () {
            headerFunctions.mainMenu.addClass('header__main-menu--open');
            $('body').css('position', 'fixed');
        });

        headerFunctions.bt_menuClose.on('click', function () {
            headerFunctions.mainMenu.removeClass('header__main-menu--open');
            $('body').css('position', 'static');
        });
    },

    openSearch: function () {

        headerFunctions.bt_search.on('click', function (e) {
            e.preventDefault();
            headerFunctions.searchForm.addClass('open');
            $(this).addClass('medium-up--hide');
        });
    }

}

headerFunctions.openCloseMenu();
headerFunctions.openSearch();
var cartFunctions = {

    buttonMinus: $('.template-cart .minus'),
    buttonPlus: $('.template-cart .plus'),

    quantityChange: function() {

        cartFunctions.buttonMinus.on('click', function(event){ 
            var input = $(this).siblings('input');
            var curVal = input.val();            
            curVal > 1 ? input.attr("value", parseInt( curVal ) - 1 ) : event.preventDefault();
        });
        cartFunctions.buttonPlus.on('click', function(){
            var input = $(this).siblings('input');
            var curVal = input.val();
            input.attr("value", parseInt( curVal ) + 1 );
        });
    }
}
cartFunctions.quantityChange();
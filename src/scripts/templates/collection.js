var collectionPageFunctions = {

  sort_by: function (param) {

    $('.sort-by__option').click(function (e) {
      Shopify.queryParams.sort_by = $(this).attr('data-value');
      location.search = $.param(Shopify.queryParams).replace(/\+/g, '%20');
    });

    $('.sort-by__option').each(function () {
      var data_value = $(this).attr('data-value');
      if (data_value == param) {
        $('.sort-by__option').removeClass('active');
        $(this).addClass('active');
        return false;
      }
    });
  },

  sort_by_menu: function () {
    $('.sort-by__indicator').click(function (e) {
      if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).parent('.sort-by').removeClass('active');
      } else {
        $(this).addClass('active');
        $(this).parent('.sort-by').addClass('active');
      }
    });
  },

  filters: function () {

    var filterToggle = $('.filters__indicator .btn, .filters__close');
    var filterContainer = $('.filters');
    var filterItem = $('.filters__item');

    filterToggle.on('click', function () {
      filterContainer.hasClass('active') ? filterContainer.removeClass('active') : filterContainer.addClass('active');
      filterContainer.hasClass('active') ? $('body').css('position', 'fixed') : $('body').css('position', 'static');
    });

    filterItem.on('click', function () {
      $(this).hasClass('active') ? $(this).removeClass('active') : $(this).addClass('active');
    });

  }

}

Shopify.queryParams = {};
if (location.search.length) {
  for (var aKeyValue, i = 0, aCouples = location.search.substr(1).split('&'); i < aCouples.length; i++) {
    aKeyValue = aCouples[i].split('=');
    if (aKeyValue.length > 1) {
      Shopify.queryParams[decodeURIComponent(aKeyValue[0])] = decodeURIComponent(aKeyValue[1]);
      collectionPageFunctions.sort_by(aKeyValue[1]);
    }
  }
} else {
  var data_value = $('.sort-by__options').attr('data-value');
  collectionPageFunctions.sort_by(data_value);
}
collectionPageFunctions.sort_by_menu();
collectionPageFunctions.filters();
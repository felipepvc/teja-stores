Teja Stores Theme
============

A custom theme for Ronnie Teja's global network of stores.

Starting with [Software Store](https://software-eu.myshopify.com/)

Administration panel [here](https://software-eu.myshopify.com/admin)

Prototypes [here](https://projects.invisionapp.com/d/main#/console/14473761/300998198/preview).